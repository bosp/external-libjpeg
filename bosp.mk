
ifdef CONFIG_EXTERNAL_LIBJPEG

# Targets provided by this project
.PHONY: libjpeg clean_libjpeg

# Add this to the "external" target
external: libjpeg
clean_external: clean_libjpeg

MODULE_DIR_LIBJPEG=external/optional/libjpeg

libjpeg: setup $(BUILD_DIR)/lib/libjpeg.so.8.4.0
$(BUILD_DIR)/lib/libjpeg.so.8.4.0:
	@echo
	@echo "==== Building JPEG Library v8.4.0 ===="
	@cd $(MODULE_DIR_LIBJPEG) && \
		PATH=$(PLATFORM_PATH) \
		CXX=$(CXX) CC=$(CC) \
		./configure  \
		--host=$(PLATFORM_TARGET) \
		--prefix=$(BUILD_DIR)
	@cd $(MODULE_DIR_LIBJPEG) && \
		CXX=$(CXX) CC=$(CC) \
		PATH=$(PLATFORM_PATH) \
		make -j$(CPUS) install
	@echo

clean_libjpeg:
	@echo "==== Clean-up JPEG Library v8.4.0 ===="
	@cd $(MODULE_DIR_LIBJPEG) && \
		make clean
	@echo

else # CONFIG_EXTERNAL_LIBJPEG

libjpeg:
	$(warning LibJPEG module disabled by BOSP configuration)
	$(error BOSP compilation failed)

endif # CONFIG_EXTERNAL_LIBJPEG

